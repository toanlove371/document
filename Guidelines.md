h1. Development Process Guidelines

These should serve as general guidelines. The person ultimately responsible for a task/project, and the circumstances, can ultimately decide about their application.

{{>toc}}

h2. General guidelines

* Only ONE issue each commit.
* Make sure EACH CHANGE corresponds to the issue.
* Make sure NO CHANGE that does not correspond to the issue is commited.
* DO NOT add whitespace, formatting changes, etc to code not directly affected by the issue you're fixing.
* DO NOT commit code that is not completed.
* DO NOT leave commented code among the changes included in a commit. If it's not used, delete it.
* Run ALL UNIT/INTEGRATION TESTS before making a commit; DO NOT commit if they fail.
* DO NOT commit code that isn't working as intended.
* DO NOT commit code that doesn't work.
* When an issue is marked as resolved, it is considered DONE. DO NOT mark it as resolved if you're not convinced it's 100% ready to deploy.

h2. Code reviews

See [[Code reviews]] for a more detailed explanation on the tools and process established for code reviews.

Note that we are using a *pre-commit* code review process, that is, the changes are reviewed *before* they are commited to version control. 

h2. Resolved issue checklist

Only mark an issue as "resolved" if all these checks are true

* The features/changes specified in the issue have been implemented
* Manual tests have been performed on the features/changes specified on the issue (manual specific tests)
* Manual tests have been performed on a reasonable set of related features of the application/program (manual integration or regression tests)
* All unit tests run correctly.
* All integration tests run correctly.
* Code reviews have been succesully performed on the changes resolving the issue (see [[Code reviews]])

h2. Commit checklist

Only make a commit if all these checks are true

* Code reviews have been succesully performed on the changes resolving the issue (see [[Code reviews]])
* The code included in the commit corresponds to ONE, and only ONE IssueID (or none, if none is applicable).
* The changes implement all or a coherent part of what is indicated in the issue's description.
* No changes outside the scope of the issue will be commited.
* All unit tests run correctly.
* All integration tests run correctly.
* A concise commit message has been chosen.
* The effect of the changes has been tested manually in the application.

